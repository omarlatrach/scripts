#!/usr/bin/env python

import vlc
import glob
import os
import time

files = []
os.chdir(".")
for f in glob.glob("*.srt"):
    files.append(f)

i = 1
for f in files:
    print("{:02.0f}- {}".format(i, f))
    i += 1

try:
    i = int(input(
        "\nFrom the above list enter the number of the file to shift (default 1): "))
except:
    i = 0

subtitles_file = files[i - 1]
video_file = subtitles_file.replace("srt", "mkv")

try:
    additional_seconds = int(
        input("Please enter the number of seconds to shift the subtitles (default 1): "))
except:
    additional_seconds = 1


def shift_time(time):
    time_array = time.split(":")
    hours = int(time_array[0])
    minutes = int(time_array[1])
    seconds = int(time_array[2].split(",")[0]) + additional_seconds
    m_seconds = time_array[2].split(",")[1]
    if seconds >= 60:
        minutes += seconds // 60
        seconds = seconds % 60
    if seconds <= -1:
        minutes -= abs(seconds) // 60 + 1
        seconds += 60
    if minutes > 60:
        hours += minutes // 60
        minutes = minutes % 60
    if minutes <= -1:
        hours -= abs(minutes) // 60 + 1
        minutes += 60
    return "{:02.0f}:{:02.0f}:{:02.0f},{}".format(hours, minutes, seconds, m_seconds)


with open(subtitles_file, "r+") as f:
    old = f.readlines()
    f.seek(0)
    for line in old:
        if " --> " in line:
            parts = line.split(" --> ")
            f.write(
                "{} --> {}".format(shift_time(parts[0]), shift_time(parts[1])))
            continue
        f.write(line)

Instance = vlc.Instance('--fullscreen')
player = Instance.media_player_new()
Media = Instance.media_new(
    video_file)
# Media.get_mrl()
Media.add_option('start-time=690.0')
# Media.add_option('run-time=60.0')
Media.add_option('stop-time=750.0')
player.set_media(Media)
player.play()

time.sleep(10)
while player.is_playing():
    pass
