#!/usr/bin/env python

from selenium import webdriver
import re
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
import time
import sys
import winsound
import requests
from shutil import copyfile
import os.path

url = "https://www.upwork.com/ab/jobs/search/?page={}&sort=recency"
today = datetime.today().strftime('%Y-%m-%d')
yesterday = datetime.strftime(datetime.now() - timedelta(1), '%Y-%m-%d')
browser = webdriver.Firefox()
browser.maximize_window()
browser.implicitly_wait(60)


def extract_links(p_number):
    try:
        browser.get(url.format(p_number))
    except Exception as e:
        notify()
        print("\n\nThe exception message:\n{}\n\n".format(e))
        input("There is a connection problem while trying to fetch: {}\nPlease try again later!".format(
            url.format(p_number)))
        sys.exit()
    html_source = str(BeautifulSoup(browser.page_source,
                                    features="html.parser").encode('utf-8'))
    return re.findall(r'href="/job/(.*?)"', html_source)


def init_links_file(links, file):
    with open('links.html', 'w+') as f:
        for i in range(len(links)):
            links[i] = "https://www.upwork.com/job/"+links[i]
            f.write('{}- <a target="_blank" href="{}">{}</a><br><br>\n'.format(today, links[i],
                                                                               links[i].split('/')[-2].split('_')[0].replace('-', ' ')))


def load_jobs(file):
    if os.path.isfile(file):
        copyfile(file, file+'.backup')
    pre_links = []
    try:
        with open(file) as f:
            for line in f:
                if yesterday in line or today in line:
                    pre_links.append(line)
                else:
                    break
            for i in range(len(pre_links)):
                pre_links[i] = re.search('job/(.*)"', pre_links[i]).group(1)
    except Exception as e:
        print("\n\nThe exception message:\n{}\n\n".format(e))
        links = []
        for p in range(30):
            links.extend(extract_links(p))
            time.sleep(3)
        init_links_file(links, file)
        notify()
        browser.close()
        sys.exit()
    return pre_links


def load_skills(file):
    if os.path.isfile(file):
        copyfile(file, file+'.backup')
    skills = {}
    try:
        with open(file) as f:
            lines = f.readlines()
            for line in lines:
                skills[line.split(" ==> ")[0].split(' ')[1]] = int(
                    line.split(" ==> ")[1])
    except:
        pass
    return skills


def notify():
    frequency = 2500  # Set Frequency To 2500 Hertz
    duration = 1000  # Set Duration To 1000 ms == 1 second
    winsound.Beep(frequency, duration)


def add_jobs(file, skills_dict):
    pre_links = load_jobs(file)
    today = datetime.today().strftime('%Y-%m-%d at %H:%M')
    new_links = []
    page_n = 1
    green = True
    while green:
        links = extract_links(page_n)
        with open(file, 'r+') as f:
            pre_content = f.read()
            f.seek(0, 0)
            for i in range(len(links)):
                if links[i] in pre_links:
                    green = False
                    notify()
                    input(
                        "This job: {}\nwas already loaded; check it out to make sure!\nPress 'Enter' key to confirm...".format(links[i].split('_')[0].replace('-', ' ')))
                    break
                links[i] = "https://www.upwork.com/job/"+links[i]
                print("Fetching ==> {}".format(links[i]))
                try:
                    r = requests.get(links[i])
                except:
                    notify()
                    print(
                        "\nConnection closed by the remote host for: {}".format(links[i]))
                    notify()
                # time.sleep(1)
                if r.ok:
                    new_skills = re.findall(r'm-xs-bottom">(.*?)\<', r.text)
                    for skill in new_skills:
                        if skill in skills_dict:
                            skills_dict[skill] += 1
                        else:
                            skills_dict[skill] = 1
                    f.write('{}- <a target="_blank" href="{}">{}</a> ==> {}<br><br>\n'.format(today, links[i],
                                                                                              links[i].split('/')[-2].split('_')[0].replace('-', ' '), new_skills))
                else:
                    notify()
                new_links.append(links[i])
            f.write(pre_content)
        page_n += 1
    return skills_dict


def update_skills_dict(skills_file):
    skills_dict = load_skills("skills_dict.txt")
    skills_dict = add_jobs("links.html", skills_dict)
    sorted_skills = sorted(skills_dict.items(),
                           key=lambda x: x[1], reverse=True)
    with open(skills_file, 'w+') as f:
        index = 1
        for skill in sorted_skills:
            f.write("{}- {} ==> {}\n".format(index, skill[0], skill[1]))
            index += 1


update_skills_dict("skills_dict.txt")
notify()
browser.close()
